26. Пішов та й загинув...
27. Якби знала, що покине, —
28. Була б не любила;
29. Якби знала, що загине, —
30. Була б не пустила;
31. Якби знала, не ходила б
32. Пізно за водою,
33. Не стояла б до півночі
34. З милим під вербою;
35. Якби знала!..